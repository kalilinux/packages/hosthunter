Source: hosthunter
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13), dh-python, python3-all
Standards-Version: 4.6.2
Homepage: https://github.com/SpiderLabs/HostHunter
Vcs-Browser: https://gitlab.com/kalilinux/packages/hosthunter
Vcs-Git: https://gitlab.com/kalilinux/packages/hosthunter.git
Rules-Requires-Root: no

Package: hosthunter
Architecture: all
Depends: chromium-driver,
         python3-fake-useragent (>= 1.1.1),
         python3-openssl,
         python3-requests,
         python3-selenium,
         python3-urllib3,
         ${misc:Depends},
         ${python3:Depends}
Description: tool to discover and extract hostnames providing a set of target IP addresses
 This package contains a tool to efficiently discover and extract hostnames
 providing a large set of target IP addresses. HostHunter utilises simple OSINT
 techniques to map IP addresses with virtual hostnames. It generates a CSV or
 TXT file containing the results of the reconnaissance.
 .
 Latest version of HostHunter also takes screenshots of the targets, it is
 currently a beta functionality.
